function getCustomer(){
  var url= "https://services.odata.org/V4/Northwind/Northwind.svc/customers";
  var request = new XMLHttpRequest();
  request.onreadystatechange = function(){
    if(this.readyState==4 && this.status==200){
      //console.log(request.responseText);
      productosobtenidos = request.responseText;
      procesarCutomer();
    }
  }
  request.open("GET",url,true);
  request.send();
}

function procesarCutomer() {
  var JSONProductos= JSON.parse(productosobtenidos);
  var divTabla=document.getElementById("divTabla");
  var tabla=document.createElement("table");
  var tbody=document.createElement("tbody");
  tabla.classList.add("table");
  tabla.classList.add("table-striped");
  var tcabecera=document.createElement("thead-dark");
  var nuevaFila2=document.createElement("tr");
  var columnatitulo1=document.createElement("th");
  columnatitulo1.innerText="Nombre";
  var columnatitulo2=document.createElement("th");
  columnatitulo2.innerText="ciudad";
  var columnatitulo3=document.createElement("th");
  columnatitulo3.innerText="bandera";
  for (var i = 0; i < JSONProductos.value.length; i++) {
    //console.log(JSONProductos.value[i].ProductName);
    var nuevaFila=document.createElement("tr");
    var columnaNombre=document.createElement("td");
    columnaNombre.innerText= JSONProductos.value[i].ContactName;
    var columnaCiudad=document.createElement("td");
    columnaCiudad.innerText= JSONProductos.value[i].City;
    var columnaBandera=document.createElement("td");
    var imagenbandera =document.createElement("img");
    imagenbandera.classList.add("flag");
    var pais=JSONProductos.value[i].Country;
    if(pais== "UK"){pais="United-Kingdom"}else{
      pais=JSONProductos.value[i].Country;
    }
    var imagen= "https://www.countries-ofthe-world.com/flags-normal/flag-of-"+pais+".png";
    imagenbandera.src=imagen;
    columnaBandera.appendChild(imagenbandera);
  //  JSONProductos.value[i].UnitsInStock;UK

    nuevaFila.appendChild(columnaNombre);
    nuevaFila.appendChild(columnaCiudad);
    nuevaFila.appendChild(columnaBandera);
    tbody.appendChild(nuevaFila);
  }
  nuevaFila2.appendChild(columnatitulo1);
  nuevaFila2.appendChild(columnatitulo2);
  nuevaFila2.appendChild(columnatitulo3);
  tabla.appendChild(nuevaFila2);
  tabla.appendChild(tbody);
  divTabla.appendChild(tabla);
//  alert(JSONProductos.value[0].ProductName);
}
